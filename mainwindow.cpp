#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gtile.h"
#include <cmath>
#include <QMessageBox>
#include "settings.h"

namespace {
    double dist(int x1, int y1, int x2, int y2)
    {
        return sqrt(pow(double(x1 - x2), 2) + pow(double(y1-y2), 2));
    }

    double dist(Point p1, Point p2)
    {
        return dist(p1.mX, p1.mY, p2.mX, p2.mY);
    }
    double dist(Player* p, Ghost* g)
    {
        return dist(p->getRealX(), p->getRealY(), g->getRealX(), g->getRealY());
    }
}
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , mScene(new QGraphicsScene(this))
    , mMap(mScene)
{
    ui->setupUi(this);
    mScene = new QGraphicsScene(this);
    ui->graphicsView->setScene(mScene);
    ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);

    mScene->setSceneRect(0,0,380,440);
    QPen obramowanie = QPen(Qt::black);
    obramowanie.setWidth(6);

    QLineF GornaGranica(mScene->sceneRect().topLeft(),mScene->sceneRect().topRight());
    QLineF PrawaGranica(mScene->sceneRect().topRight(),mScene->sceneRect().bottomRight());
    QLineF DolnaGranica(mScene->sceneRect().bottomRight(),mScene->sceneRect().bottomLeft());
    QLineF LewaGranica(mScene->sceneRect().bottomLeft(),mScene->sceneRect().topLeft());


    mScene->addLine(GornaGranica,obramowanie);
    mScene->addLine(PrawaGranica,obramowanie);
    mScene->addLine(DolnaGranica,obramowanie);
    mScene->addLine(LewaGranica,obramowanie);

    ui->graphicsView->setSceneRect(mScene->sceneRect());

    mMap = Map(mScene);
    mPlayer = new Player(mMap.mTiles[starting_player_x][starting_player_y], mScene);
    mPlayer->update();

    mGhostBlue = new Ghost(mMap.mTiles[starting_ghost_blue_x][starting_ghost_blue_y], mScene, mPlayer, blue);
    mGhostGreen = new Ghost(mMap.mTiles[starting_ghost_green_x][starting_ghost_green_x], mScene, mPlayer, green);
    mGhostRed = new Ghost(mMap.mTiles[starting_ghost_red_x][starting_ghost_red_y], mScene, mPlayer, red);

    // setting timer
    mStepTimer = new QTimer(this);
//    mStepTimer->start(15);

    connect(mStepTimer, SIGNAL(timeout()),
            this, SLOT(make_step()));

    mSuperdotTimer = new QTimer(this);
    connect(mSuperdotTimer, SIGNAL(timeout()),
            this, SLOT(superdot_ended()));
    this->setFocus();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Left:
        mPlayer->mRequestedDirection = LEFT;
        break;

    case Qt::Key_Right:
        mPlayer->mRequestedDirection = RIGHT;
        break;

    case Qt::Key_Down:
        mPlayer->mRequestedDirection = DOWN;
        break;

    case Qt::Key_Up:
        mPlayer->mRequestedDirection = UP;
        break;

    default:
        qDebug() <<  "wrong key";
        break;
    }
}
void MainWindow::make_step()
{
    this->setFocus();

    ui->scor_value->setText(QString::number(mPlayer->mScore));


    mGhostBlue->makeStep();
    mGhostGreen->makeStep();
    mGhostRed->makeStep();
    if(mPlayer->pro && !mSuperdotTimer->isActive())
    {
        mSuperdotTimer->start(superdot_time_ms);
    }

    // check for colision
    bool dead = false;
    if(dist(mPlayer, mGhostBlue) < (GHOST_SIZE/2 + PAC_SIZE/2))
    {
        if(mPlayer->pro && mGhostBlue->mAlive) // has superdot
        {
            mGhostBlue->mAlive = false;
            mPlayer->mScore += 25;
        }else if(mGhostBlue->mAlive)
            dead=true;
    }
    if(dist(mPlayer, mGhostGreen) < (GHOST_SIZE/2 + PAC_SIZE/2)){
        if(mPlayer->pro && mGhostGreen->mAlive) // has superdot
        {
            mGhostGreen->mAlive = false;
            mPlayer->mScore += 25;
        }else if(mGhostGreen->mAlive)
            dead=true;
    }
    if(dist(mPlayer, mGhostRed) < (GHOST_SIZE/2 + PAC_SIZE/2))
    {
        if(mPlayer->pro && mGhostRed->mAlive) // has superdot
        {
            mGhostRed->mAlive = false;
            mPlayer->mScore += 25;
        }else if(mGhostRed->mAlive)
            dead=true;
    }


    if(dead)
    {
        mStepTimer->stop();
        QMessageBox::warning(this,"warn", "koniec");
    }else
    {
        mPlayer->makeStep();
    }
    mScene->update();
}

void MainWindow::superdot_ended()
{
    mPlayer->pro = false;
}

void MainWindow::on_pushButton_clicked()
{
    qDebug("starting");
    mStepTimer->start(step_time);
}

void MainWindow::on_stop_button_clicked()
{
    qDebug("stopping");
    mStepTimer->stop();
}


void MainWindow::on_reset_button_clicked()
{
    qDebug("resetting");

    mStepTimer->stop();

    mSuperdotTimer->stop();

    // resetting the player
    delete mPlayer;
    mPlayer = new Player(mMap.mTiles[starting_player_x][starting_player_y], mScene);
    mPlayer->update();

    //resetting the ghosts
    delete mGhostBlue;
    mGhostBlue = new Ghost(mMap.mTiles[starting_ghost_blue_x][starting_ghost_blue_y], mScene, mPlayer, blue);

    delete mGhostGreen;
    mGhostGreen = new Ghost(mMap.mTiles[starting_ghost_green_x][starting_ghost_green_x], mScene, mPlayer, green);

    delete mGhostRed;
    mGhostRed = new Ghost(mMap.mTiles[starting_ghost_red_x][starting_ghost_red_y], mScene, mPlayer, red);

    // filling the map
    for (int x =0; x < mMap.mSizeX; x++)
    {
        for(int y=0; y< mMap.mSizeY; y++)
        {
            if(!mMap.mTiles[x][y]->mIsWall && !mMap.mTiles[x][y]->mHadSuperdot)
                mMap.mTiles[x][y]->mHasDot = true;
            else if(mMap.mTiles[x][y]->mHadSuperdot)
                mMap.mTiles[x][y]->mHasSuperdot = true;
            mMap.mTiles[x][y]->update();
        }
    }
    this->setFocus();
}
