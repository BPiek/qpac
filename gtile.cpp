#include "gtile.h"
#include <QTextStream>
#include <QFile>
#include <QString>
#include <iostream>
#include <QMessageBox>
#include <QDebug>

GTile::GTile(int x, int y, bool wall, QGraphicsScene *scenePtr)
    : Tile(x,y,wall)
{
        mScenePtr = scenePtr;
        mScenePtr->addItem(this);
        mNeighbour = new GTile*[DIRECTION_SIZE];
}

GTile::~GTile()
{
    delete mNeighbour;
}

QRectF GTile::boundingRect() const
{
    return QRect(mPosition.mX*BASIC_SIZE,mPosition.mY*BASIC_SIZE,BASIC_SIZE,BASIC_SIZE);
}

void GTile::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    if(mIsWall)
    {
        QBrush brushWall = QBrush(Qt::black);
        painter->setBrush(brushWall);

        painter->drawRect(mPosition.mX*BASIC_SIZE,
                          mPosition.mY*BASIC_SIZE,
                          BASIC_SIZE,
                          BASIC_SIZE);
    }else if(mResp)
    {
        QBrush brushResp = QBrush(Qt::darkYellow);

        painter->setBrush(brushResp);
        painter->setOpacity(0.5);
        painter->drawRect(mPosition.mX*BASIC_SIZE,
                          mPosition.mY*BASIC_SIZE,
                          BASIC_SIZE,
                          BASIC_SIZE);
    }
    if(mHasDot)
    {
        QBrush brushDot = QBrush(Qt::gray);
        painter->setBrush(brushDot);

        painter->drawEllipse(mPosition.mX*BASIC_SIZE + (BASIC_SIZE-DOT_SIZE)/2,
                             mPosition.mY*BASIC_SIZE + (BASIC_SIZE-DOT_SIZE)/2,
                             DOT_SIZE,
                             DOT_SIZE);
    }else if(mHasSuperdot)
    {
        QBrush brushDot = QBrush(Qt::magenta);
        painter->setBrush(brushDot);

        painter->drawEllipse(mPosition.mX*BASIC_SIZE + (BASIC_SIZE-SUPER_DOT_SIZE)/2,
                             mPosition.mY*BASIC_SIZE + (BASIC_SIZE-SUPER_DOT_SIZE)/2,
                             SUPER_DOT_SIZE,
                             SUPER_DOT_SIZE);
    }
}

int GTile::getRealX()
{
    return (mPosition.mX+0.5) * BASIC_SIZE;
}

int GTile::getRealY()
{
    return (mPosition.mY+0.5) * BASIC_SIZE;
}

void GTile::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    if(mResp)
    {
        qDebug() << "cannot change respown tile";
        return;
    }
    qDebug() << "changing state: ("<<mPosition.mX <<", " << mPosition.mY << ") ";
    if(mIsWall)
    {
        mIsWall = false;
        mHasSuperdot = false;
        mHasDot= true;
        mScenePtr->update();
    }else if(mHasDot)
    {
        mIsWall = false;
        mHasSuperdot = true;
        mHasDot= false;
        mScenePtr->update();
    }else if(mHasSuperdot)
    {
        mIsWall = false;
        mHasSuperdot = false;
        mHasDot= false;
        mScenePtr->update();
    }else if(!mIsWall && !mHasSuperdot)
    {
        mIsWall = true;
        mHasSuperdot = false;
        mHasDot= false;
        mScenePtr->update();
    }
}


void Map::reset()
{
}

void Map::printMap()
{
    for(int y =0; y < mSizeY; y++)
    {
        for(int x = 0; x < mSizeX; x++)
        {
            std::cout << (mTiles[x][y]->mIsWall ? "X":" ");
        }
        std::cout << "\n";
    }
}


Map::Map(QGraphicsScene *scene)
{
    mSizeX = 19;
    mSizeY = 22;


    QFile MapFile(":/mapa.txt");

    MapFile.open(QFile::ReadOnly | QFile::Text);

    QTextStream in(&MapFile);

    QString MapStr;
    MapStr = in.readLine();

    mTiles = new GTile**[mSizeX];
    for(int x = 0; x<mSizeX; x++)
        mTiles[x] = new GTile*[mSizeY];

    // reading map file content and creating map
    for(int x = 0; x < mSizeX; x++)
    {
        for(int y = 0; y < mSizeY; y++)
        {
            if (MapStr[y*mSizeX+x] == '0')
            {
                mTiles[x][y] = new GTile(x,y,false, scene);
            }
            else if(MapStr[y*mSizeX+x] == '1')
            {
                mTiles[x][y] = new GTile(x,y,true, scene);
            }
            else if(MapStr[y*mSizeX+x] == '2')
            {
             mTiles[x][y] = new GTile(x, y, false, scene);
             mTiles[x][y]->mHasSuperdot = true;
            }
            else{
                // some warning mby
            }
        }
    }
    MapFile.close();

    // filling the neighbours
    for(int x = 0; x < mSizeX; x++)
    {
        for(int y = 0; y < mSizeY; y++)
        {
            // setting left and right neighbours
            if(x == 0){
                mTiles[x][y]->mNeighbour[LEFT] = mTiles[mSizeX-1][y];
                mTiles[x][y]->mNeighbour[RIGHT] = mTiles[x+1][y];
            }else if(x == mSizeX-1)
            {
                mTiles[x][y]->mNeighbour[LEFT] = mTiles[x-1][y];
                mTiles[x][y]->mNeighbour[RIGHT] = mTiles[0][y];
            }else
            {
                mTiles[x][y]->mNeighbour[LEFT] = mTiles[x-1][y];
                mTiles[x][y]->mNeighbour[RIGHT] = mTiles[x+1][y];
            }

            // setting up and down neighbours
            if(y == 0){
                mTiles[x][y]->mNeighbour[UP] = mTiles[x][mSizeY-1];
                mTiles[x][y]->mNeighbour[DOWN] = mTiles[x][y+1];
            }else if( y == mSizeY -1)
            {
                mTiles[x][y]->mNeighbour[UP] = mTiles[x][y-1];
                mTiles[x][y]->mNeighbour[DOWN] = mTiles[x][0];
            }else
            {
                mTiles[x][y]->mNeighbour[UP] = mTiles[x][y-1];
                mTiles[x][y]->mNeighbour[DOWN] = mTiles[x][y+1];
            }
        }
    }
}

Map::~Map()
{

}
