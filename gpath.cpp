#include "gpath.h"
#include <cmath>
#include <QDebug>

GPath::GPath(QGraphicsScene* scene, Ghost* ghost)
    : mScene(scene)
    , mGhost(ghost)
    , mShow(true)
{
    mScene->addItem(this);
}

void GPath::generateGPath(int startX, int startY, QVector<GTile *> vectToGO)
{
    // cleaning old
    mPathLines.clear();

    // settingnew
    int prevX = startX;
    int prevY = startY;
    foreach (GTile* gtile, vectToGO)
    {

        QLine newLine(prevX, prevY, gtile->getRealX(), gtile->getRealY());
        mPathLines.append(newLine);
        prevX = gtile->getRealX();
        prevY = gtile->getRealY();
    }
}



QRectF GPath::boundingRect() const
{
//    int minx=10000;
//    int miny=10000;
//    int maxx=0;
//    int maxy=0;

//    foreach (QLine lineItem, mPathLines)
//    {
//        // minx
//        minx = std::min(minx, lineItem.x1());
//        minx = std::min(minx, lineItem.x2());

//        // miny
//        miny = std::min(miny, lineItem.y1());
//        miny = std::min(miny, lineItem.y2());

//        // maxx
//        maxx = std::max(maxx, lineItem.x1());
//        maxx = std::max(maxx, lineItem.x2());

//        // maxy
//        maxy = std::max(maxy, lineItem.y1());
//        maxy = std::max(maxy, lineItem.y2());
//    }
    return QRectF(0,0,1100,1100);

//    return QRectF(minx,miny, maxx-minx, maxy-miny);
}

void GPath::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if(!mShow)
        return;

    QBrush brushPath;// = QBrush(Qt::yellow);
    switch(mGhost->mType)
    {
    case green:
        brushPath = QBrush(Qt::green);
        painter->setPen(QPen(Qt::green, 2, Qt::DashDotLine, Qt::RoundCap));
        break;

    case blue:
        brushPath = QBrush(Qt::blue);
        painter->setPen(QPen(Qt::blue, 2, Qt::DashDotLine, Qt::RoundCap));
        break;

    case red:
        brushPath = QBrush(Qt::red);
        painter->setPen(QPen(Qt::red, 2, Qt::DashDotLine, Qt::RoundCap));
        break;
    }


    painter->setBrush(brushPath);

    if(mPathLines.size() > 0)
    {
        painter->drawLines(mPathLines);
    }
    mScene->update();
}

void GPath::clear()
{
    mPathLines.clear();
}
void GPath::updatePath()
{

    generateGPath(mGhost->getRealX() + GHOST_SIZE/2, mGhost->getRealY()+GHOST_SIZE/2, mGhost->mPathToGo);
}
