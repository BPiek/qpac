#ifndef SETTINGS_H
#define SETTINGS_H

#endif // SETTINGS_H
static const int green_vel = 3;
static const int blue_vel = 3;
static const int red_vel = 4;
static const int player_vel = 8;
static const int multiply = 1.3;

static const int superdot_time_ms = 8000;
static const int step_time = 15;

static const int resp_x = 9;
static const int resp_y = 10;

static const int starting_player_x = 1;
static const int starting_player_y = 2;

static const int starting_ghost_blue_x = 4;
static const int starting_ghost_blue_y = 10;

static const int starting_ghost_green_x = 6;
static const int starting_ghost_green_y = 10;

static const int starting_ghost_red_x = 14;
static const int starting_ghost_red_y = 10;
