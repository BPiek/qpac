#include "tile.h"

#include <QTextStream>
#include <QFile>
#include <QString>
#include <iostream>
#include "settings.h"

Tile::Tile(int x, int y, bool wall)
    : mPosition(x,y)
    , mIsWall(wall)
    , mHasDot(!wall)
    , mHasSuperdot(false)
    , mHadSuperdot(false)
    , mResp(false)
{
    if(x == resp_x && y == resp_y)
    {
        mResp = true;
    }
}


Point::Point(int x, int y)
    : mX(x)
    , mY(y)
{
}


Tile::~Tile()
{
}


