#-------------------------------------------------
#
# Project created by QtCreator 2015-01-02T23:49:10
#
#-------------------------------------------------

QT       += core gui
CONFIG += ordered
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qpac
TEMPLATE = app


SOURCES += \
    tile.cpp \
    gtile.cpp \
    character.cpp \
    ghost.cpp \
    gpath.cpp \
    mainwindow.cpp \
    main.cpp

HEADERS  += mainwindow.h \
            tile.h \
    gtile.h \
    character.h \
    ghost.h \
    settings.h \
    gpath.h

FORMS    += mainwindow.ui

RESOURCES += \
    res.qrc
