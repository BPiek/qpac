#include "ghost.h"
#include "settings.h"
#include "mainwindow.h"
#include "tile.h"
#include <QDebug>
#include <queue>


namespace {
    Direction getDirectionToGo(GTile* startingTile, GTile* nextTile)
    {
        for(int i=0; i<DIRECTION_SIZE; i++)
        {
            if(startingTile->mNeighbour[i] == nextTile)
            {
                switch(i)
                {
                case(UP):
                    return UP;
                    break;
                case(DOWN):
                    return DOWN;
                    break;
                case(LEFT):
                    return LEFT;
                    break;
                case (RIGHT):
                    return RIGHT;
                    break;
                }
            }
        }
        qDebug() << "WARNING getDirectionToGo: given tiles are NOT neighbours!";

        return UP; //should not occure!! ( make exception? )
    }

}

Ghost::Ghost(GTile *startingTile, QGraphicsScene *scenePtr, Player *playerPtr, ghost_type type)
    : mOccupiedTilePtr(startingTile)
    , mPosition(startingTile->mPosition.mX, startingTile->mPosition.mY)
    , mHeadingDirection(RIGHT)
    , mRequestedDirection(UP)
    , mRunAway(false)
    , mPercentageOfNextStep(0)
    , mType(type)
    , mPlayerPtr(playerPtr)
    , mAlive(true)
{
    switch(mType)
    {
    case green:
        mSpeedPerTurn = green_vel*multiply;
        break;

    case blue:
        mSpeedPerTurn = blue_vel*multiply;
        break;

    case red:
        mSpeedPerTurn = red_vel*multiply;
        break;
    }

    mScenePtr = scenePtr;
    mScenePtr->addItem(this);
    update();

    // gPath
    mGPath = new GPath(mScenePtr, this);

}

Ghost::~Ghost()
{
    delete mGPath;
}

void Ghost::makeStep()
{
    if(mOccupiedTilePtr->mNeighbour[mHeadingDirection]->mIsWall)
    {
        mOccupiedTilePtr->update();
        mPercentageOfNextStep = 0;
        //jak mial przed soba sciane, ustawialo requested direction, ktore bylo na sztywno up
        //wiec jak wpadl na sciane biegnac w gore to nie mial szans sie z tego wydostac
        //bo za kazdym razem wpadal w tego ifa...

        for(int i = 0 ; i < DIRECTION_SIZE; i++){
            mRequestedDirection = Direction(i);
            if(!mOccupiedTilePtr->mNeighbour[mRequestedDirection]->mIsWall) break;
        }
        mHeadingDirection = mRequestedDirection;
    }else
    {
        mPercentageOfNextStep += mSpeedPerTurn;

        if(mPercentageOfNextStep >=100)
        {
            mOccupiedTilePtr = mOccupiedTilePtr->mNeighbour[mHeadingDirection];
//            mHeadingDirection = mRequestedDirection;

            mPosition.mX = mOccupiedTilePtr->mPosition.mX;
            mPosition.mY = mOccupiedTilePtr->mPosition.mY;
            mPercentageOfNextStep = 0;
            if(mPathToGo.size() >= 2)
                mHeadingDirection = getDirectionToGo(mPathToGo[0], mPathToGo[1]);
            else{
//                qDebug() << "WARN makeStep: there is no path to execute";
            }
        updatePath();
        }

    }

    mGPath->updatePath();
}

int Ghost::getRealX()
{
    if(mHeadingDirection == LEFT || mHeadingDirection == RIGHT)
        return (mPosition.mX  + (mHeadingDirection == LEFT ? -1:1) * (mPercentageOfNextStep/100.0)) * BASIC_SIZE;
    else
        return mPosition.mX * BASIC_SIZE;
}

int Ghost::getRealY()
{
    if(mHeadingDirection == UP || mHeadingDirection == DOWN)
        return (mPosition.mY + (mHeadingDirection == DOWN ? 1:-1) * (mPercentageOfNextStep/100.0)) * BASIC_SIZE;
    else
        return mPosition.mY * BASIC_SIZE;
}

QRectF Ghost::boundingRect() const
{
    return QRect(mPosition.mX*BASIC_SIZE,mPosition.mY*BASIC_SIZE,BASIC_SIZE,BASIC_SIZE);

}

void Ghost::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QBrush brushGhost;// = QBrush(Qt::yellow);
    switch(mType)
    {
    case green:
        brushGhost = QBrush(Qt::green);
        break;

    case blue:
        brushGhost = QBrush(Qt::blue);
        break;

    case red:
        brushGhost = QBrush(Qt::red);
        break;
    }

    painter->setBrush(brushGhost);
    if(mAlive)
        painter->setOpacity(0.85);
    else
        painter->setOpacity(0.3);

    int size = GHOST_SIZE * (mAlive ? 1:0.6);
    painter->drawEllipse(getRealX() + (BASIC_SIZE - size)/2,
                         getRealY() + (BASIC_SIZE - size)/2,
                         size,
                         size);
}

GTile *Ghost::getNextTile()
{
    if(mPercentageOfNextStep == 0)
        return mOccupiedTilePtr;
    return mOccupiedTilePtr->mNeighbour[mHeadingDirection];
}

bool Ghost::updatePath()
{

    mPathToGo = find_path();
//qDebug() << "duch: " << this->mType;
//qDebug() << "track";
//for(size_t i =0; i< mPathToGo.size(); i++){
//  qDebug() << mPathToGo[i]->mPosition.mX << " " <<mPathToGo[i]->mPosition.mY;
//}
    if(mPathToGo.size() == 0)
        return false;
    if(mOccupiedTilePtr == mPathToGo[0])
    {
        mPathToGo.removeAt(0);
        //emit pathUpdated();
        return true;
    }
    return false;
}

//find path to go with A* algorithm

QVector<GTile*> Ghost::find_path(){

    //debug stuff
    static int pliki=0;
    pliki++;

    QVector<GTile*> final_path;
    Point  mDestination = get_destination();

//    qDebug() << "ghost: " << this->mType;
//    qDebug() << "Position: " << this->mPosition.mX << "  " << this->mPosition.mY;
//    qDebug() << "pos2: " << mOccupiedTilePtr->mPosition.mX << "  " <<mOccupiedTilePtr->mPosition.mY;
//    qDebug() << "pac position: " << mPlayerPtr->mPosition.mX << " " << mPlayerPtr->mPosition.mY;
//    qDebug() << "Destination: " << mDestination.mX << "  " << mDestination.mY;

    double openNodes[19][22];
    int closedNodes[19][22];
    GTile* prevTileMap[19][22];

    static Node *startNode, *currNode, *neighbourNode;
    startNode = new Node(mOccupiedTilePtr, 0);
    startNode->findPriority(mDestination);

    std::priority_queue<Node> queue[2];
    bool index = 0;
    for (int y=0;y<22;y++){
            for (int x=0;x<19;x++){
                closedNodes[x][y]=0;
                openNodes[x][y]=0;
            }
    }
    queue[index].push(*startNode);
    openNodes[startNode->xPos][startNode->yPos] = startNode->getPriority();
    // do while there are nodes to check
    while(!queue[index].empty()){
        currNode = new Node(queue[index].top().currTile, queue[index].top().traveledDistance);
        queue[index].pop();

        openNodes[currNode->xPos][currNode->yPos] = 0;
        closedNodes[currNode->xPos][currNode->yPos] = 1;

        //if destination reached
        if(currNode->xPos == mDestination.mX && currNode->yPos == mDestination.mY){
        //odczytanie wyniku
            int x = currNode->xPos;
            int y = currNode->yPos;
//            qDebug() << "destination riched";
            do{

                GTile* tileToPush = prevTileMap[x][y];
                final_path.push_front(tileToPush);
                x = tileToPush->mPosition.mX;
                y = tileToPush->mPosition.mY;

            }while(!(x == startNode->currTile->mPosition.mX)
                   || !(y == startNode->currTile->mPosition.mY));
            delete currNode;
            while(!queue[index].empty()){
                queue[index].pop();
            }
//            qDebug() << prevTileMap[4][10]->mPosition.mX << " " << prevTileMap[4][10]->mPosition.mY;

            return final_path;
        }

        else{
            //check all possible moves to neighbour
         for(int i = 0; i < DIRECTION_SIZE; i++){
             int neighbourX = currNode->currTile->mNeighbour[i]->mPosition.mX;
             int neighbourY = currNode->currTile->mNeighbour[i]->mPosition.mY;
             bool wall = currNode->currTile->mNeighbour[i]->mIsWall;
             //if not wall and not on closed list
             if(!wall && !closedNodes[neighbourX][neighbourY]){
                 neighbourNode = new Node(currNode->currTile->mNeighbour[i], currNode->traveledDistance);
                 neighbourNode->update_distance();
                 neighbourNode->findPriority(mDestination);
/*                 qDebug() << " sprawdzane: " << currNode->currTile->mPosition.mX << "  " << currNode->currTile->mPosition.mY;
                 qDebug() << "sasiad: "  << neighbourX << "  " << neighbourY;
                 qDebug() << "priorytet sasiada " << neighbourNode->getPriority();
*/                 //if not in open - add to open nodes map
                 if(openNodes[neighbourX][neighbourY]==0){
                     openNodes[neighbourX][neighbourY] = neighbourNode->getPriority();
                     queue[index].push(*neighbourNode);
                     prevTileMap[neighbourX][neighbourY] = currNode->currTile;
                 }
                 //if came to same node through other way and it is better one
                 else if(openNodes[neighbourX][neighbourY] > neighbourNode->getPriority()){
                     openNodes[neighbourX][neighbourY] = neighbourNode->getPriority();
                     prevTileMap[neighbourX][neighbourY] = currNode->currTile;
                     while (!((queue[index].top().xPos==neighbourX) && (queue[index].top().yPos==neighbourY))){
                         queue[1-index].push(queue[index].top());
                        queue[index].pop();
                  }
                     if(queue[index].size()>queue[1-index].size()){ //??? is this extra check necessary?
                          index=1-index; //index switch 1->0 or 0->1
                    }

                     while(!queue[index].empty()){
                          queue[1-index].push(queue[index].top());
                          queue[index].pop();
                     }
                     index = 1-index;
                     queue[index].push(*neighbourNode);
                 }
                 else delete neighbourNode;
             }
          }
         delete currNode;
        }
    }
    final_path.push_back(mOccupiedTilePtr);
    return final_path;
}


//get destination (terminal) point depending on strategy and situation
Point Ghost::get_destination(){
    Point dest(mPlayerPtr->mPosition);
/*    if(this->mAlive && !mPlayerPtr->pro){
        if(mPlayerPtr->mHeadingDirection==LEFT || mPlayerPtr->mHeadingDirection == RIGHT){
            int coef = mPlayerPtr->mHeadingDirection == LEFT ? -1 : 1;
            dest.mX += coef;

        }
        if(mPlayerPtr->mHeadingDirection==DOWN || mPlayerPtr->mHeadingDirection == UP){
            int coef = mPlayerPtr->mHeadingDirection == UP ? -1 : 1;
            dest.mY += coef;
        }
    }
*/      if(this->mAlive && mPlayerPtr->pro){
        bool isLeft = mPlayerPtr->mPosition.mX < 10 ? 1:0;
        bool isUp = mPlayerPtr->mPosition.mY < 11 ? 1:0;
        if(isLeft && isUp) dest = Point(18, 21);
        else if(!isLeft && isUp) dest = Point(1, 21);
        else if(!isLeft && !isUp) dest = Point(1, 1);
        else dest = Point(18, 1);
    }
    else if(!this->mAlive){
        dest.mX = resp_x;
        dest.mY = resp_y;
    }
    return dest;
}


Node::Node(GTile *curr, double dist){
    xPos = curr->mPosition.mX;
    yPos = curr->mPosition.mY;
    currTile = curr;
    traveledDistance=dist;
}

double Node::getDistance(int xDest, int yDest){

    double straight_distance = sqrt(pow(double(xDest-xPos),2)+pow(double(yDest-yPos),2));
    int g_to_wall;
    int wall_to_d;

    bool left_tp = (xDest - xPos <= 0 ? 0 : 1); //which tp shoul ghost use

    if(left_tp){
        g_to_wall = xPos;
        wall_to_d = 19 - xDest;        //map is 19x22
    }
    else{
        g_to_wall = 19 - xPos;
        wall_to_d = xDest;
    }
    double through_wall_distance = sqrt(pow(double(g_to_wall+wall_to_d),2) + pow(double(yDest-yPos),2));
return straight_distance;
    return qMin(straight_distance, through_wall_distance);
}

