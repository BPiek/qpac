#include "character.h"
#include <assert.h>
#include <iostream>
#include <QDebug>
#include "settings.h"

Player::Player(GTile *startingTile, QGraphicsScene* scenePtr)
    : mOccupiedTilePtr(startingTile)
    , mPosition(startingTile->mPosition.mX, startingTile->mPosition.mY)
    , mHeadingDirection(DOWN)
    , mRequestedDirection(DOWN)
    , mScore(0)
    , speedPerTurn(player_vel * multiply)
    , pro(false)
    , percentageOfNextStep(0)
{
//    qDebug() << "tile x: "<< mPosition.mX <<" y: "<< mPosition.mY << (mOccupiedTilePtr->mIsWall ? "wall" : "not wall");
    mScenePtr = scenePtr;
    mScenePtr->addItem(this);
//    setFlag(ItemIsMovable);
    update();
}

Player::~Player()
{

}

void Player::makeStep()
{
    if(mOccupiedTilePtr->mNeighbour[mHeadingDirection]->mIsWall)
    {
        mOccupiedTilePtr->update();
        percentageOfNextStep = 0;
        mHeadingDirection = mRequestedDirection;
    }else
    {
        percentageOfNextStep += speedPerTurn;

        if(percentageOfNextStep >=100)
        {
            mOccupiedTilePtr = mOccupiedTilePtr->mNeighbour[mHeadingDirection];
            if(mOccupiedTilePtr->mHasDot)
            {
                mScore ++;
                mOccupiedTilePtr->mHasDot = false;
            }
            if(mOccupiedTilePtr->mHasSuperdot)
            {
                mScore += 10;
                pro=true;
                mOccupiedTilePtr->mHasSuperdot =false;
                mOccupiedTilePtr->mHadSuperdot =true;
            }
            mHeadingDirection = mRequestedDirection;

            mPosition.mX = mOccupiedTilePtr->mPosition.mX;
            mPosition.mY = mOccupiedTilePtr->mPosition.mY;
            percentageOfNextStep = 0;
        }
    }
}

int Player::getRealX()
{
    if(mHeadingDirection == LEFT || mHeadingDirection == RIGHT)
        return (mPosition.mX  + (mHeadingDirection == LEFT ? -1:1) * (percentageOfNextStep/100.0)) * BASIC_SIZE ;
    else
        return mPosition.mX * BASIC_SIZE;
}

int Player::getRealY()
{
    if(mHeadingDirection == UP || mHeadingDirection == DOWN)
        return (mPosition.mY + (mHeadingDirection == DOWN ? 1:-1) * (percentageOfNextStep/100.0)) * BASIC_SIZE;
    else
        return mPosition.mY * BASIC_SIZE;
}

QRectF Player::boundingRect() const
{
    return QRect(mPosition.mX*BASIC_SIZE,mPosition.mY*BASIC_SIZE,BASIC_SIZE,BASIC_SIZE);
}

void Player::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    QBrush brushPac ;

    if(pro)
        brushPac = QBrush(Qt::cyan);
    else
        brushPac = QBrush(Qt::yellow);
    painter->setBrush(brushPac);

    painter->drawEllipse(getRealX(),
                         getRealY(),
                         PAC_SIZE,
                         PAC_SIZE);
}
