#ifndef GTILE_H
#define GTILE_H

#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include "tile.h"

static const int BASIC_SIZE = 20;
static const int DOT_SIZE = 0.4 * BASIC_SIZE;
static const int SUPER_DOT_SIZE = 0.7*BASIC_SIZE;

class GTile : public QGraphicsItem, public Tile
{
public:
    GTile(int x,int y, bool wall, QGraphicsScene* scenePtr);
    ~GTile();
    GTile **mNeighbour;

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    int getRealX();
    int getRealY();

    QGraphicsScene* mScenePtr;
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

class Map
{
public:
    Map(QGraphicsScene* scene);
    ~Map();

    int mSizeX;
    int mSizeY;

    GTile *** mTiles;

    void reset();
    void printMap();
};

#endif // GTILE_H
