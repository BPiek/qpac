#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QtCore>
#include <QtGui>
#include <QTimer>
#include "character.h"
#include "ghost.h"
#include "gpath.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QGraphicsScene *mScene;

    QTimer* mStepTimer;
    QTimer* mSuperdotTimer;

    Player* mPlayer;

    Ghost* mGhostGreen;
    Ghost* mGhostBlue;
    Ghost* mGhostRed;

    Map mMap;

    //controls:
    void keyPressEvent(QKeyEvent *even);

private slots:
    void make_step();
    void superdot_ended();

    void on_pushButton_clicked();
    void on_stop_button_clicked();
    void on_reset_button_clicked();
};

#endif // MAINWINDOW_H
