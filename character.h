#ifndef CHARACTER_H
#define CHARACTER_H

#include "gtile.h"

static const int PAC_SIZE = BASIC_SIZE;
class Player: public QGraphicsItem
{
public:
    Player(GTile* startingTile, QGraphicsScene* scenePtr);
    ~Player();

    GTile* mOccupiedTilePtr;
    Point mPosition; // x y in "tile" word (not for display)

    Direction mHeadingDirection;
    Direction mRequestedDirection;

    int mScore;
    int speedPerTurn; // percentage of full step ( 1 tile step )
    bool pro; // can eat ghosts
    int percentageOfNextStep;

    void makeStep();
    int getRealX();
    int getRealY();

    // from QGraphicsItem
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    QGraphicsScene* mScenePtr;
};

#endif // CHARACTER_H
