#ifndef GPATH_H
#define GPATH_H

#include "gtile.h"
#include "ghost.h"
#include <QVector>
class Ghost;
class GPath : public QGraphicsItem
{
public:
    GPath(QGraphicsScene* scene, Ghost* ghost);

    QGraphicsScene* mScene;
    QVector<QLine> mPathLines;

    void generateGPath(int startX, int startY, QVector<GTile*> vectToGO);
    Ghost* mGhost;
    bool mShow;

    // from QGraphicsItem
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void clear();
    void updatePath();
};

#endif // GPATH_H
