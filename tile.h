#ifndef TILE_H
#define TILE_H

class Point
{
public:
    Point(int x, int y);
    int mX;
    int mY;
};
enum Direction
{
    UP = 0,
    DOWN,
    RIGHT,
    LEFT,
    DIRECTION_SIZE = 4
};

class Tile
{
public:
    Tile(int x, int y, bool wall);
    ~Tile();

    Point mPosition;
    bool mIsWall;
    bool mHasDot;
    bool mHasSuperdot;
    bool mHadSuperdot;

    bool mResp;

    bool mCanTeleport;
    Direction mTeleportDirection; // for display purpose


};





#endif // TILE_H
