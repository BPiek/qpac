#ifndef GHOST_H
#define GHOST_H

#include "character.h"
#include <map>
#include <QVector>
#include "gpath.h"

class GPath;
static const int GHOST_SIZE = BASIC_SIZE;
enum ghost_type
{
      green =0
    , blue
    , red
};

class Ghost : public QGraphicsItem
{
public:
    Ghost(GTile *startingTile, QGraphicsScene *scenePtr, Player *playerPtr, ghost_type type);
    ~Ghost();

    GTile* mOccupiedTilePtr;
    Point mPosition;
    QVector<GTile*> mPathToGo;

    Direction mHeadingDirection;
    Direction mRequestedDirection;

    bool mRunAway;
    int mSpeedPerTurn;
    int mPercentageOfNextStep;
    ghost_type mType;
    Player* mPlayerPtr;
    bool mAlive;

    // methods
    void makeStep();
    int getRealX();
    int getRealY();
    GPath* mGPath;
    QGraphicsScene* mScenePtr;

    // from QGraphicsItem
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    // for path making
    GTile* getNextTile();
    bool updatePath();
    void drawPath();

private:
    QVector<QVector<GTile*> > paths;
    Point get_destination();
    void get_paths(GTile *currPosition);
    QVector<GTile*> find_path();
};

class Node{
 public:
    Node(GTile* curr, double dist);
    GTile* currTile;
    int xPos;
    int yPos;
    double traveledDistance;
    double priority;
    double getPriority() const {return priority;}

    void update_distance(){traveledDistance+=2;}
    void findPriority(Point Destination)
    {
        priority = traveledDistance + getDistance(Destination.mX, Destination.mY);
    }
    bool operator<(const Node &b) const { return this->getPriority() > b.getPriority(); }
private:
    double getDistance(int xDest, int yDest);

};

#endif // GHOST_H
